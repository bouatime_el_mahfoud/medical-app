package com.example.theblackme.applicationmedical;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ContactsActivity extends AppCompatActivity {
    private  ListView listecontacts;
    private LinearLayout empty;
    private RequestQueue requestQueue;
    private StringRequest request;
    int iddoctor;
    private String urlv="http://10.23.18.106:7080/appmedical/getpatientsofdoctors.php";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contacts_listview);
        Bundle extras=getIntent().getExtras();
        if(extras!=null){
            iddoctor=extras.getInt("id_doctor");
        }
        listecontacts=(ListView)findViewById(R.id.listviewcontacts);
        final LinearLayout empty=(LinearLayout)findViewById(R.id.emptyview_contacts);
        requestQueue= Volley.newRequestQueue(this);
        final ArrayList<Patient> list=new ArrayList<>();
        request=new StringRequest(Request.Method.POST, urlv, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{

                    JSONObject objet=new JSONObject(response);
                    Patient prd;
                    JSONArray log=objet.getJSONArray("login");
                    for (int i=0;i<log.length();i++) {

                        JSONObject com=log.getJSONObject(i);
                        prd=new Patient(com.getInt("id_patient"),com.getString("nom"),com.getString("prenom"),com.getString("telephone"),com.getInt("sex"),com.getInt("age"),com.getString("adresse"));
                        list.add(prd);
                    }
                    ContactsArrayAdapter cmdAdapter=new ContactsArrayAdapter(ContactsActivity.this,0,list);

                    listecontacts.setEmptyView(empty);
                    listecontacts.setAdapter(cmdAdapter);


                }
                catch (JSONException e){
                    Log.e("erroe json","un erreur s'est produit leur de la conversion du json");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ContactsActivity.this,"une erreur s'est produit coté base de donnée",Toast.LENGTH_LONG).show();
                System.out.println(error);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> params=new HashMap<>();
                params.put("iddoctor",Integer.toString(iddoctor));
                return params;
            }
        };
        requestQueue.add(request);
        listecontacts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Patient p=list.get(i);
                Intent move=new Intent(ContactsActivity.this,DiscussionActivity.class);
                move.putExtra("id_doctor",1);
                String fullname=p.getNom()+" "+p.getPrenom();
                int id_patient=p.getId_patient();
                move.putExtra("id_patient",id_patient);
                move.putExtra("fullname",fullname);
                startActivity(move);
                finish();
            }
        });
    }
}
