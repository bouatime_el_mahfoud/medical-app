package com.example.theblackme.applicationmedical;

import java.util.Date;

/**
 * Created by theblackme on 06/06/2018.
 */

public class Message  {

    private  String message;
    private String heure ;
    private int etat;

    public Message( String message, String heure, int etat) {
         this.message = message;
        this.heure = heure;
        this.etat = etat;
    }



    public String getMessage() {
        return message;
    }

    public String getHeure() {
        return heure;
    }

    public int getEtat() {
        return etat;
    }



    public void setMessage(String message) {
        this.message = message;
    }

    public void setHeure(String heure) {
        this.heure = heure;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }
}
