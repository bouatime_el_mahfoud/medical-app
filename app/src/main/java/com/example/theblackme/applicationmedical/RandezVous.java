package com.example.theblackme.applicationmedical;

import java.util.Date;

/**
 * Created by theblackme on 05/06/2018.
 */

public class RandezVous {
    private int id_randezvous;
    private String sujet;
    private String date;
    private int id_patient;
    private String nom;
    private String prenom;
    private String address;


    public RandezVous(int id_randezvous, String sujet, int id_patient, String nom, String prenom,String address,String date) {
        this.id_randezvous = id_randezvous;
        this.sujet = sujet;
        this.date = date;
        this.id_patient = id_patient;
        this.nom = nom;
        this.prenom = prenom;
        this.address=address;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getId_randezvous() {
        return id_randezvous;
    }

    public String getSujet() {
        return sujet;
    }

    public String getDate() {
        return date;
    }

    public int getId_patient() {
        return id_patient;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setId_randezvous(int id_randezvous) {
        this.id_randezvous = id_randezvous;
    }

    public void setSujet(String sujet) {
        this.sujet = sujet;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setId_patient(int id_patient) {
        this.id_patient = id_patient;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
}
