package com.example.theblackme.applicationmedical;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by theblackme on 30/05/2018.
 */

public class HomeFragementPagerAdapter extends FragmentPagerAdapter {
    public HomeFragementPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public Fragment getItem(int position) {
        if (position == 0) {
            return new ProfileFragement();
        } else  {
            return new DashbordFragement();
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}
