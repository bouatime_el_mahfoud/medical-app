package com.example.theblackme.applicationmedical;


import android.app.Dialog;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class UsersconsultingFragement extends Fragment implements TextToSpeech.OnInitListener {
    Dialog consultUser;
    private ListView listeUtilisateurs;
    private String urlv="http://10.23.18.106:7080/appmedical/getpatientsofdoctors.php";
    private String urla="http://10.23.18.106:7080/appmedical/deletemespatients.php";
    private RequestQueue requestQueue;
    private StringRequest request;
    private int iddoctor=1;
    private String message;
    private TextToSpeech tts;

    public UsersconsultingFragement() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View root=inflater.inflate(R.layout.users_listview,container,false);
        listeUtilisateurs=(ListView)root.findViewById(R.id.listview_usersadd);
        final LinearLayout empty=(LinearLayout) root.findViewById(R.id.emptyview);
        requestQueue= Volley.newRequestQueue(getActivity());
        consultUser=new Dialog(getContext());
        final ArrayList<Patient> list=new ArrayList<>();
        request=new StringRequest(Request.Method.POST, urlv, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{

                    JSONObject objet=new JSONObject(response);
                    Patient prd;
                    JSONArray log=objet.getJSONArray("login");
                    for (int i=0;i<log.length();i++) {

                        JSONObject com=log.getJSONObject(i);
                        prd=new Patient(com.getInt("id_patient"),com.getString("nom"),com.getString("prenom"),com.getString("telephone"),com.getInt("sex"),com.getInt("age"),com.getString("adresse"));
                        list.add(prd);
                    }
                    PatientArrayAdapter cmdAdapter=new PatientArrayAdapter(getActivity(),0,list);

                    listeUtilisateurs.setEmptyView(empty);
                    listeUtilisateurs.setAdapter(cmdAdapter);


                }
                catch (JSONException e){
                    Log.e("erroe json","un erreur s'est produit leur de la conversion du json");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(),"une erreur s'est produit coté base de donnée",Toast.LENGTH_LONG).show();
                System.out.println(error);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> params=new HashMap<>();
                params.put("iddoctor",Integer.toString(iddoctor));
                return params;
            }
        };
        requestQueue.add(request);
        listeUtilisateurs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (tts != null) {
                    tts.stop();
                    tts.shutdown();
                    tts = null;
                }
                consultUser.setContentView(R.layout.pop_up_user_infos);
                final Patient p=list.get(i);
                TextView fullname=(TextView) consultUser.findViewById(R.id.user_name_popup);
                TextView address=(TextView) consultUser.findViewById(R.id.user_address_popup);
                address.setText(p.getAddress());
                TextView telephone=(TextView) consultUser.findViewById(R.id.user_phone_popup);
                TextView sexe=(TextView) consultUser.findViewById(R.id.user_sexe_popup);
                TextView age=(TextView) consultUser.findViewById(R.id.user_age_popup);
                TextView logout=(TextView) consultUser.findViewById(R.id.popup_close_consulting) ;
                logout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        consultUser.dismiss();
                    }
                });

                Button supprimer=(Button) consultUser.findViewById(R.id.user_delete_button_popup);
                fullname.setText(p.getNom()+" "+p.getPrenom());
                telephone.setText(p.getTelephone());
                int sexev=p.getSexe();
                if(sexev==0){
                    sexe.setText("F");
                }
                else if(sexev==1){
                    sexe.setText("M");
                }
                age.setText(Integer.toString(p.getAge()));
                supprimer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        /*


                         */
                        requestQueue= Volley.newRequestQueue(getActivity());
                        request=new StringRequest(Request.Method.POST, urla, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Toast.makeText(getActivity(),"le patient est supprimé de votre liste",Toast.LENGTH_LONG).show();
                                getActivity().recreate();
                                consultUser.dismiss();

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getActivity(),"une erreur s'est produit coté base de donnée",Toast.LENGTH_LONG).show();
                                System.out.println(error);
                            }
                        }){
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                HashMap<String,String> params=new HashMap<>();
                                params.put("idmedcin",Integer.toString(iddoctor));
                                params.put("idpatient",Integer.toString(p.getId_patient()));
                                return params;
                            }
                        };
                        requestQueue.add(request);

                        /*






                         */

                    }
                });

                consultUser.show();

            }

            /* il reste du travail à faire*/
        });
        listeUtilisateurs.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                Patient p=list.get(i);
                if (tts != null) {
                    tts.stop();
                    tts.shutdown();
                    tts = null;
                }
                message="pour consulter les informations du patient ,ou supprimer le patient :"+p.getNom()+" "+p.getPrenom()+"  de votre liste des patients,veuillez cliquer sur ce bouton";
                tts=new TextToSpeech(getActivity(), UsersconsultingFragement.this);
                speakOut();
                return true;
            }
        });
        empty.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (tts != null) {
                    tts.stop();
                    tts.shutdown();
                    tts = null;
                }
                message="votre liste des patients est vide";
                tts=new TextToSpeech(getActivity(), UsersconsultingFragement.this);
                speakOut();
                return true;
            }
        });
        return root;

    }
    @Override
    public void onInit(int i) {
        if (i == TextToSpeech.SUCCESS) {

            int result = tts.setLanguage(Locale.FRANCE);

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {

                speakOut();
            }

        } else {
            Log.e("TTS", "Initilization Failed!");
        }

    }
    private void speakOut() {


        tts.speak(message, TextToSpeech.QUEUE_FLUSH, null);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (tts != null) {
            tts.stop();
            tts.shutdown();
            tts = null;
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser) {
            if (tts != null) {
                tts.stop();
                tts.shutdown();
                tts = null;
            }
        }
    }


}
