package com.example.theblackme.applicationmedical;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class GestionMessagerie extends AppCompatActivity {
private  ListView listDiscussion;
    private String urlv="http://10.23.18.106:7080/appmedical/getdisscussionsdoctor.php";
    private RequestQueue requestQueue;
    private StringRequest request;
    private FloatingActionButton nouveaumessage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.disscusions_listview);
        listDiscussion=(ListView)findViewById(R.id.discussionlist);
        final TextView empty=(TextView)findViewById(R.id.emptytextdisscussion) ;
        nouveaumessage=(FloatingActionButton)findViewById(R.id.nouveaumessage);
        nouveaumessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent move=new Intent(GestionMessagerie.this,ContactsActivity.class);
                move.putExtra("id_doctor",1);
                startActivity(move);
            }
        });
        requestQueue= Volley.newRequestQueue(this);
        final ArrayList<Disscusion> list=new ArrayList<>();

        request=new StringRequest(Request.Method.POST, urlv, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{

                    JSONObject objet=new JSONObject(response);
                    Disscusion prd;
                    JSONArray log=objet.getJSONArray("login");
                    for (int i=0;i<log.length();i++) {

                        JSONObject com=log.getJSONObject(i);




                            prd=new Disscusion(com.getInt("id_patientmessage"),com.getString("messagefinal"),com.getString("nompatient"),com.getString("prenompatient"),com.getString("heuremessage"));
                            list.add(prd);
                            DiscussionArrayAdapter cmdAdapter=new DiscussionArrayAdapter(GestionMessagerie.this,0,list);
                            listDiscussion.setEmptyView(empty);
                            listDiscussion.setAdapter(cmdAdapter);


                    }



                }
                catch (JSONException e){
                    Log.e("erroe json","un erreur s'est produit leur de la conversion du json");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(GestionMessagerie.this,"une erreur s'est produit coté base de donnée",Toast.LENGTH_LONG).show();
                System.out.println(error);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> params=new HashMap<>();
                params.put("iddoctor",Integer.toString(1));
                return params;
            }
        };
        requestQueue.add(request);
        listDiscussion.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent move=new Intent(GestionMessagerie.this,DiscussionActivity.class);
                move.putExtra("id_doctor",1);
                Disscusion d=list.get(i);
                String fullname=d.getNom()+" "+d.getPrenom();
                int id_patient=d.getId_patientmessage();
                move.putExtra("id_patient",id_patient);
                move.putExtra("fullname",fullname);
                startActivity(move);
            }
        });

    }
}
