package com.example.theblackme.applicationmedical;


import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashbordFragement extends Fragment implements TextToSpeech.OnInitListener{
    private CardView settings,gestionpatients,gestionhoraires,systememessagerie,gestionmedicaments;
    private TextToSpeech tts;
    String message;
    public DashbordFragement() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root= inflater.inflate(R.layout.activity_main,container,false);
        settings=(CardView)root.findViewById(R.id.settings);
        gestionhoraires=(CardView) root.findViewById(R.id.horairesdespatients);
        gestionpatients=(CardView) root.findViewById(R.id.gestionpatients) ;
        systememessagerie=(CardView) root.findViewById(R.id.systeme_messagerie);
        gestionmedicaments=(CardView) root.findViewById(R.id.gestionmedicaments) ;
        gestionpatients.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent usersgestion=new Intent(getContext(),GestionPatients.class);
                startActivity(usersgestion);
            }
        });
        settings.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (tts != null) {
                    tts.stop();
                    tts.shutdown();
                }
                 message="parametres.ce menu vous permet de changer les parametres de votre compte";
                 tts=new TextToSpeech(getActivity(), DashbordFragement.this);
                speakOut();
                return true;
            }
        });


        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent settingsmenu=new Intent(getContext(),SettingsActivity.class);
                startActivity(settingsmenu);

            }
        });

        gestionhoraires.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent horairesgestion=new Intent(getContext(),GestionHoraires.class);
                startActivity(horairesgestion);
            }
        });
        systememessagerie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent messagerie=new Intent(getContext(),GestionMessagerie.class);
                startActivity(messagerie);
            }
        });
        gestionmedicaments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent medicaments=new Intent(getContext(),GestionMedicament.class);
                startActivity(medicaments);
            }
        });
        gestionpatients.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (tts != null) {
                    tts.stop();
                    tts.shutdown();
                }
                message="gestion des patients.ce menu,vous permet: ajouter des patients à votre liste des patient,supprimer des patients de votre liste des patient et d'accepter les invitations  des patients souhaitant  devenir votre patient. ";
                tts=new TextToSpeech(getActivity(), DashbordFragement.this);
                speakOut();
                return true;
            }
        });
        gestionmedicaments.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (tts != null) {
                    tts.stop();
                    tts.shutdown();
                }
                message="gestion des médicaments.ce menu vous permet:la consultation des médicaments,la spécification des médicaments,et la suppression d'un médicament  déja spécifiée  pour un patient ";
                tts=new TextToSpeech(getActivity(), DashbordFragement.this);
                speakOut();
                return true;
            }
        });
        gestionhoraires.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (tts != null) {
                    tts.stop();
                    tts.shutdown();
                }
                message="gestion des rendez-vous.ce menu vous permet:la consultation des rendez-vous,la spécification des rendez-vous,et la suppression d'un rendez-vous  déja spécifiée  pour un patient ";
                tts=new TextToSpeech(getActivity(), DashbordFragement.this);
                speakOut();
                return true;
            }
        });
        systememessagerie.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (tts != null) {
                    tts.stop();
                    tts.shutdown();
                }
                message="messages des patients.ce menu vous permet:la communication avec vos patients. ";
                tts=new TextToSpeech(getActivity(), DashbordFragement.this);
                speakOut();
                return true;
            }
        });

        return root;
    }

    @Override
    public void onInit(int status) {

        if (status == TextToSpeech.SUCCESS) {

            int result = tts.setLanguage(Locale.FRANCE);

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {

                speakOut();
            }

        } else {
            Log.e("TTS", "Initilization Failed!");
        }

    }
    private void speakOut() {


        tts.speak(message, TextToSpeech.QUEUE_FLUSH, null);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (tts != null) {
            tts.stop();
            tts.shutdown();
            tts=null;
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser){
            if (tts != null) {
                tts.stop();
                tts.shutdown();
                tts = null;
            }
        }
    }
}
