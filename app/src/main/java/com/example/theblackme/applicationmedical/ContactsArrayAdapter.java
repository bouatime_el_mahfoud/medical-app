package com.example.theblackme.applicationmedical;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by theblackme on 10/06/2018.
 */

public class ContactsArrayAdapter extends ArrayAdapter<Patient> {
    public ContactsArrayAdapter(@NonNull Context context, int resource, ArrayList<Patient> patients) {
        super(context, resource, patients);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View root=convertView;
        if(convertView==null){
            root= LayoutInflater.from(getContext()).inflate(R.layout.new_message_item,parent,false);
        }
        Patient patient=getItem(position);
        TextView nom=root.findViewById(R.id.add_contact_nom);
        nom.setText(patient.getNom());
        TextView prenom=root.findViewById(R.id.add_contact_prenom);
        prenom.setText(patient.getPrenom());
        return root;

    }
}
