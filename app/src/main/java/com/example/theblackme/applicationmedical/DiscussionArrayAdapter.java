package com.example.theblackme.applicationmedical;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by theblackme on 06/06/2018.
 */

public class DiscussionArrayAdapter extends ArrayAdapter<Disscusion> {
    public DiscussionArrayAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Disscusion> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
         View root=convertView;
         if(convertView==null){
             root= LayoutInflater.from(getContext()).inflate(R.layout.discussion_item,parent,false);
             
         }
         Disscusion disscusion=getItem(position);
        TextView name=(TextView)root.findViewById(R.id.contact_name);
         TextView message=(TextView)root.findViewById(R.id.message);
         name.setText(disscusion.getNom()+" "+disscusion.getPrenom());
         message.setText(disscusion.getMessage());
        return root;
    }
}
