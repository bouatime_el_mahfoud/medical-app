package com.example.theblackme.applicationmedical;

/**
 * Created by theblackme on 06/06/2018.
 */

public class MedicamentPatient {
    private int id_medicament;
    private int id_patient;
    private String nom_medicament;
    private String nom_patient;
    private String prenom_patient;

    public MedicamentPatient(int id_medicament, int id_patient, String nom_medicament, String nom_patient, String prenom_patient) {
        this.id_medicament = id_medicament;
        this.id_patient = id_patient;
        this.nom_medicament = nom_medicament;
        this.nom_patient = nom_patient;
        this.prenom_patient = prenom_patient;
    }

    public int getId_medicament() {
        return id_medicament;
    }

    public int getId_patient() {
        return id_patient;
    }

    public String getNom_medicament() {
        return nom_medicament;
    }

    public String getNom_patient() {
        return nom_patient;
    }

    public String getPrenom_patient() {
        return prenom_patient;
    }

    public void setId_medicament(int id_medicament) {
        this.id_medicament = id_medicament;
    }

    public void setId_patient(int id_patient) {
        this.id_patient = id_patient;
    }

    public void setNom_medicament(String nom_medicament) {
        this.nom_medicament = nom_medicament;
    }

    public void setNom_patient(String nom_patient) {
        this.nom_patient = nom_patient;
    }

    public void setPrenom_patient(String prenom_patient) {
        this.prenom_patient = prenom_patient;
    }
}
