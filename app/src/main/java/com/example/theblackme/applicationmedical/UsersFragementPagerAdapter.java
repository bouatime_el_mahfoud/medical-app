package com.example.theblackme.applicationmedical;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by theblackme on 05/06/2018.
 */

public class UsersFragementPagerAdapter extends FragmentPagerAdapter {

   public UsersFragementPagerAdapter(FragmentManager fm){
       super(fm);
   }
    public Fragment getItem(int position) {
        if (position == 0) {
            return new UserAddFragement();
        }
        else if(position==1){
            return new UsersconsultingFragement();
        }
        else if(position==2){
            return new UsersInvitationFragement();
        }
        else  {
            return null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "ajouter patient";
            case 1:
                return "mes patients";
            case 2:
                return "invitaions";
            default:
                return null;
        }
    }
}
