package com.example.theblackme.applicationmedical;

/**
 * Created by theblackme on 11/06/2018.
 */

public class InfosMedecin {
    private int id_medcin;
    private String nom;
    private String prenom;
    private String date_naissance;
    private String mail;
    private String telephone;
    private String specialie;
    private int invitations;
    private int patients;
    private String adresse;

    public InfosMedecin(int id_medcin, String nom, String prenom, String date_naissance, String mail, String telephone, String specialie, int invitations, int patients, String adresse) {
        this.id_medcin = id_medcin;
        this.nom = nom;
        this.prenom = prenom;
        this.date_naissance = date_naissance;
        this.mail = mail;
        this.telephone = telephone;
        this.specialie = specialie;
        this.invitations = invitations;
        this.patients = patients;
        this.adresse = adresse;
    }

    public int getId_medcin() {
        return id_medcin;
    }

    public void setId_medcin(int id_medcin) {
        this.id_medcin = id_medcin;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getDate_naissance() {
        return date_naissance;
    }

    public void setDate_naissance(String date_naissance) {
        this.date_naissance = date_naissance;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getSpecialie() {
        return specialie;
    }

    public void setSpecialie(String specialie) {
        this.specialie = specialie;
    }

    public int getInvitations() {
        return invitations;
    }

    public void setInvitations(int invitations) {
        this.invitations = invitations;
    }

    public int getPatients() {
        return patients;
    }

    public void setPatients(int patients) {
        this.patients = patients;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }
}
