package com.example.theblackme.applicationmedical;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by theblackme on 05/06/2018.
 */

public class MedicamentsFragementManagerAdapter extends FragmentPagerAdapter {
    public MedicamentsFragementManagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new MedicamentConsultingFragment();
            case 1:
                return  new MedicamentAddFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "medicaments";
            case 1:
                return "specifier medicaments";
            default:
                return null;
        }
        }
}

