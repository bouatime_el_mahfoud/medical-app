package com.example.theblackme.applicationmedical;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by theblackme on 05/06/2018.
 */

public class PatientDemandeArrayAsapter extends ArrayAdapter<PatientDemande> {
    public PatientDemandeArrayAsapter(@NonNull Context context, int resource, @NonNull ArrayList<PatientDemande> objects) {
        super(context, resource, objects);
    }
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View root=convertView;
        if(convertView==null){
            root= LayoutInflater.from(getContext()).inflate(R.layout.usersinvitation_items,parent,false);
        }
        PatientDemande patient=getItem(position);
        TextView nom=root.findViewById(R.id.user_invitation_nom);
        nom.setText(patient.getNom());
        TextView prenom=root.findViewById(R.id.user_invitation_prenom);
        prenom.setText(patient.getPrenom());
        return root;

    }
}
