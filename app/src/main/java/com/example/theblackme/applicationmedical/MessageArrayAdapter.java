package com.example.theblackme.applicationmedical;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by theblackme on 06/06/2018.
 */

public class MessageArrayAdapter extends ArrayAdapter<Message> {
    public MessageArrayAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Message> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Message message=getItem(position);


        View root=convertView;
        if(message.getEtat()==0){
            root=LayoutInflater.from(getContext()).inflate(R.layout.sendmessage_item,parent,false);
            TextView messagecontent=(TextView)root.findViewById(R.id.message_sent);
            messagecontent.setText(message.getMessage());
            TextView dateenvoie=(TextView)root.findViewById(R.id.message_date_sent);
            dateenvoie.setText(message.getHeure());

            return root;
        }
        else if (message.getEtat()==1){
            root=LayoutInflater.from(getContext()).inflate(R.layout.recievemessage_item,parent,false);



            TextView messagerecv=(TextView) root.findViewById(R.id.message_recived);
            messagerecv.setText(message.getMessage());
            TextView datercv=(TextView)root.findViewById(R.id.message_date_recived);
            datercv.setText(message.getHeure());

            return root;
        }
        return root;

    }
}
