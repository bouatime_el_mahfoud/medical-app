package com.example.theblackme.applicationmedical;


import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragement extends Fragment implements TextToSpeech.OnInitListener {
    private ImageView deconnexion;
    private RequestQueue requestQueue;
    private StringRequest request;
    private TextToSpeech tts;
    private ImageView profile;
    private String message;
    private LinearLayout menu_email,menu_phone,menu_specialite,menu_date;
    private RelativeLayout menu_invitation,menupatients;
    private String urlv="http://10.23.18.106:7080/appmedical/getinfosmedecin.php";
    private int iddoctor=1;
    private TextView fullname,adresse,patients,invitations,email,phone,specialite,datenaissance;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
            final View root=inflater.inflate(R.layout.profile,container,false);
            fullname=(TextView)root.findViewById(R.id.name_of_the_doctor) ;
            adresse=(TextView)root.findViewById(R.id.address_doctor) ;
            patients=(TextView)root.findViewById(R.id.patients) ;
            invitations=(TextView)root.findViewById(R.id.invitations) ;
            email=(TextView)root.findViewById(R.id.email_medecin) ;
            menu_email=(LinearLayout)root.findViewById(R.id.menu_email);
            menu_phone=(LinearLayout)root.findViewById(R.id.menu_telephone) ;
            menu_specialite=(LinearLayout) root.findViewById(R.id.menu_specialité);
            menu_date=(LinearLayout)root.findViewById(R.id.menu_date_de_naisance) ;
            phone=(TextView) root.findViewById(R.id.telephone_medecin);
            specialite=(TextView) root.findViewById(R.id.specialite) ;
            datenaissance=(TextView) root.findViewById(R.id.date_de_naissance);
            menu_invitation=(RelativeLayout)root.findViewById(R.id.menu_invitations);
            menupatients=(RelativeLayout)root.findViewById(R.id.menu_patients) ;
            deconnexion=(ImageView)root.findViewById(R.id.deconnexion);
            profile=(ImageView)root.findViewById(R.id.profileimage);
            deconnexion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   getActivity().finish();
                }
            });


        /*


         */


        return root;

    }

    @Override
    public void onResume() {
        super.onResume();
        requestQueue= Volley.newRequestQueue(getActivity());
        /*



         */
        request=new StringRequest(Request.Method.POST, urlv, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{

                    JSONObject objet=new JSONObject(response);
                    Patient prd;
                    JSONArray log=objet.getJSONArray("login");



                    JSONObject com=log.getJSONObject(0);
                    JSONObject com1=log.getJSONObject(1);
                    JSONObject com2=log.getJSONObject(2);
                    InfosMedecin medecin=new InfosMedecin(com.getInt("id_medecin"),com.getString("nom"),com.getString("prenom"),com.getString("date_naissance"),com.getString("mail"),com.getString("telephone"),com.getString("specialite"),com1.getInt("invitations"),com2.getInt("patients"),com.getString("adresse"));
                    fullname.setText(medecin.getNom()+" "+medecin.getPrenom());
                    adresse.setText(medecin.getAdresse());
                    invitations.setText(Integer.toString(medecin.getInvitations()));
                    patients.setText(Integer.toString(medecin.getPatients()));
                    email.setText(medecin.getMail());
                    datenaissance.setText(medecin.getDate_naissance());
                    phone.setText(medecin.getTelephone());
                    specialite.setText(medecin.getSpecialie());




                }
                catch (JSONException e){
                    Log.e("erroe json","un erreur s'est produit leur de la conversion du json");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(),"une erreur s'est produit coté base de donnée",Toast.LENGTH_LONG).show();
                System.out.println(error);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> params=new HashMap<>();
                params.put("iddoctor",Integer.toString(iddoctor));
                return params;
            }
        };
        requestQueue.add(request);
        profile.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (tts != null) {
                    tts.stop();
                    tts.shutdown();
                    tts = null;
                }
                message="Bienvenu dans votre profile médecin :"+fullname.getText().toString()+",pour acceder au menu de gestion veuillez glisser à gauche.";
                tts=new TextToSpeech(getActivity(), ProfileFragement.this);
                speakOut();
                return true;
            }

    });
        adresse.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (tts != null) {
                    tts.stop();
                    tts.shutdown();
                    tts = null;
                }
                message="votre adresse est: "+adresse.getText().toString();
                tts=new TextToSpeech(getActivity(), ProfileFragement.this);
                speakOut();
                return true;
            }
        });
        fullname.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (tts != null) {
                    tts.stop();
                    tts.shutdown();
                    tts = null;
                }
                message="votre nom complet est: "+fullname.getText().toString();
                tts=new TextToSpeech(getActivity(), ProfileFragement.this);
                speakOut();
                return true;
            }
        });
        menu_invitation.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (tts != null) {
                    tts.stop();
                    tts.shutdown();
                    tts = null;
                }
                message="le nombre  d'invitations effectuée par des patients souhaitant devenir votre patient est: "+invitations.getText().toString()+"invitations";
                tts=new TextToSpeech(getActivity(), ProfileFragement.this);
                speakOut();
                return true;
            }
        });
        menupatients.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (tts != null) {
                    tts.stop();
                    tts.shutdown();
                    tts = null;
                }
                message="le nombre de vos patients est: "+patients.getText().toString()+"patients";
                tts=new TextToSpeech(getActivity(), ProfileFragement.this);
                speakOut();
                return true;
            }
        });
       menu_email.setOnLongClickListener(new View.OnLongClickListener() {
           @Override
           public boolean onLongClick(View view) {
               if (tts != null) {
                   tts.stop();
                   tts.shutdown();
                   tts = null;
               }
               message="votre email est: "+email.getText().toString();
               tts=new TextToSpeech(getActivity(), ProfileFragement.this);
               speakOut();
               return true;
           }
       });
       menu_phone.setOnLongClickListener(new View.OnLongClickListener() {
           @Override
           public boolean onLongClick(View view) {
               if (tts != null) {
                   tts.stop();
                   tts.shutdown();
                   tts = null;
               }
               message="votre numero de telephone est: "+phone.getText().toString();
               tts=new TextToSpeech(getActivity(), ProfileFragement.this);
               speakOut();
               return true;
           }
       });
       menu_specialite.setOnLongClickListener(new View.OnLongClickListener() {
           @Override
           public boolean onLongClick(View view) {
               if (tts != null) {
                   tts.stop();
                   tts.shutdown();
                   tts = null;
               }
               message="votre specialité medicale est : "+specialite.getText().toString();
               tts=new TextToSpeech(getActivity(), ProfileFragement.this);
               speakOut();
               return true;
           }
       });
       menu_date.setOnLongClickListener(new View.OnLongClickListener() {
           @Override
           public boolean onLongClick(View view) {
               if (tts != null) {
                   tts.stop();
                   tts.shutdown();
                   tts = null;
               }
               message="votre date de naissance est : le "+datenaissance.getText().toString();
               tts=new TextToSpeech(getActivity(), ProfileFragement.this);
               speakOut();
               return true;
           }
       });
    }

    @Override
    public void onInit(int i) {
        if (i == TextToSpeech.SUCCESS) {

            int result = tts.setLanguage(Locale.FRANCE);

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {

                speakOut();
            }

        } else {
            Log.e("TTS", "Initilization Failed!");
        }

    }
    private void speakOut() {


        tts.speak(message, TextToSpeech.QUEUE_FLUSH, null);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (tts != null) {
            tts.stop();
            tts.shutdown();
            tts = null;
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser){
            if (tts != null) {
                tts.stop();
                tts.shutdown();
                tts = null;
            }
        }
    }
}
