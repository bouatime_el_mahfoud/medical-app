package com.example.theblackme.applicationmedical;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by theblackme on 06/06/2018.
 */

public class MedicamentAddArrayAdapter extends ArrayAdapter<Patient> {
    public MedicamentAddArrayAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Patient> objects) {
        super(context, resource, objects);
    }

    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View root=convertView;
        if(convertView==null){
            root= LayoutInflater.from(getContext()).inflate(R.layout.add_prescription_item,parent,false);
        }
        Patient patient=getItem(position);
        TextView nom=root.findViewById(R.id.medicament_user_nom);
        nom.setText(patient.getNom());
        TextView prenom=root.findViewById(R.id.medicament_user_prenom);
        prenom.setText(patient.getPrenom());
        return root;

    }
}
