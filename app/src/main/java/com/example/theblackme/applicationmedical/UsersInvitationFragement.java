package com.example.theblackme.applicationmedical;


import android.app.Dialog;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class UsersInvitationFragement extends Fragment implements TextToSpeech.OnInitListener{
    private Dialog userinvitation;
    private String urlv="http://10.23.18.106:7080/appmedical/getinvitations.php";
    private String urla="http://10.23.18.106:7080/appmedical/acceptinvitation.php";
    private String urld="http://10.23.18.106:7080/appmedical/deleteinvitation.php";
    private RequestQueue requestQueue;
    private StringRequest request;
    ListView listeinvitations;
    private String message;
    private TextToSpeech tts;
    public UsersInvitationFragement() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
      final View root=inflater.inflate(R.layout.users_invitation_listview,container,false);
       listeinvitations=(ListView)root.findViewById(R.id.listview_invitations);
       final LinearLayout empty=(LinearLayout)root.findViewById(R.id.emtyview);
       userinvitation=new Dialog(getContext());
        requestQueue= Volley.newRequestQueue(getActivity());
        final ArrayList<PatientDemande> list=new ArrayList<>();
        request=new StringRequest(Request.Method.POST, urlv, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{

                    JSONObject objet=new JSONObject(response);
                    PatientDemande prd;
                    JSONArray log=objet.getJSONArray("login");
                    for (int i=0;i<log.length();i++) {

                        JSONObject com=log.getJSONObject(i);
                        prd=new PatientDemande(com.getInt("id_patient"),com.getString("nom"),com.getString("prenom"),com.getString("telephone"),com.getInt("sex"),com.getInt("age"),com.getInt("id_demande"),com.getString("adresse"));
                        list.add(prd);
                    }
                    PatientDemandeArrayAsapter cmdAdapter=new PatientDemandeArrayAsapter(getActivity(),0,list);

                    listeinvitations.setEmptyView(empty);
                    listeinvitations.setAdapter(cmdAdapter);


                }
                catch (JSONException e){
                    Log.e("erroe json","un erreur s'est produit leur de la conversion du json");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(),"une erreur s'est produit coté base de donnée",Toast.LENGTH_LONG).show();
                System.out.println(error);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> params=new HashMap<>();
                params.put("iddoctor",Integer.toString(1));
                return params;
            }
        };
        listeinvitations.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (tts != null) {
                    tts.stop();
                    tts.shutdown();
                    tts = null;
                }
                userinvitation.setContentView(R.layout.pop_up_invitation);
               final PatientDemande p=list.get(i);
                TextView fullname=(TextView) userinvitation.findViewById(R.id.user_name_popup_invite);
                TextView address=(TextView) userinvitation.findViewById(R.id.user_invit_popup_address);
                address.setText(p.getAddress());
                TextView telephone=(TextView) userinvitation.findViewById(R.id.user_popup_invite_phone);
                TextView sexe=(TextView) userinvitation.findViewById(R.id.user_popup_sexe_invite);
                TextView age=(TextView) userinvitation.findViewById(R.id.user_age_popup_invite);

                fullname.setText(p.getNom()+" "+p.getPrenom());
                telephone.setText(p.getTelephone());
                int sexev=p.getSexe();
                if(sexev==0){
                    sexe.setText("F");
                }
                else if(sexev==1){
                    sexe.setText("M");
                }
                age.setText(Integer.toString(p.getAge()));
                TextView close=(TextView) userinvitation.findViewById(R.id.user_close_invite) ;
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        userinvitation.dismiss();
                    }
                });
                Button add =(Button) userinvitation.findViewById(R.id.user_invite_addbutton);
                Button rejectinvite=(Button) userinvitation.findViewById(R.id.user_close_popup_invite);
                add.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        /*





                         */
                        requestQueue= Volley.newRequestQueue(getActivity());
                        request=new StringRequest(Request.Method.POST, urla, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Toast.makeText(getActivity(),"le patient est ajouté de votre liste",Toast.LENGTH_LONG).show();
                                getActivity().recreate();
                                userinvitation.dismiss();

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getActivity(),"une erreur s'est produit coté base de donnée",Toast.LENGTH_LONG).show();
                                System.out.println(error);
                            }
                        }){
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                HashMap<String,String> params=new HashMap<>();
                                params.put("idmedcin",Integer.toString(1));
                                params.put("idpatient",Integer.toString(p.getId_patient()));
                                return params;
                            }
                        };
                        requestQueue.add(request);
                        /*





                         */
                    }
                });
                rejectinvite.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        /*




                         */
                        requestQueue= Volley.newRequestQueue(getActivity());
                        request=new StringRequest(Request.Method.POST, urld, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Toast.makeText(getActivity(),"la demande est refusée",Toast.LENGTH_LONG).show();
                                getActivity().recreate();
                                userinvitation.dismiss();

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getActivity(),"une erreur s'est produit coté base de donnée",Toast.LENGTH_LONG).show();
                                System.out.println(error);
                            }
                        }){
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                HashMap<String,String> params=new HashMap<>();
                                params.put("idmedcin",Integer.toString(1));
                                params.put("idpatient",Integer.toString(p.getId_patient()));
                                return params;
                            }
                        };
                        requestQueue.add(request);

                        /*


                         */
                    }
                });
                userinvitation.show();
            }
        });
        requestQueue.add(request);
        listeinvitations.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                PatientDemande p=list.get(i);
                if (tts != null) {
                    tts.stop();
                    tts.shutdown();
                    tts = null;
                }
                message="pour accepter ou refuser l'invitation reçu de la part du patient:"+p.getNom()+" "+p.getPrenom()+" .veuillez cliquez sur ce bouton";
                tts=new TextToSpeech(getActivity(), UsersInvitationFragement.this);
                speakOut();
                return true;
            }
        });
        empty.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                message="votre liste des invitations est vide ";
                tts=new TextToSpeech(getActivity(), UsersInvitationFragement.this);
                speakOut();
                return true;
            }
        });
        return root;
    }
    @Override
    public void onInit(int i) {
        if (i == TextToSpeech.SUCCESS) {

            int result = tts.setLanguage(Locale.FRANCE);

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {

                speakOut();
            }

        } else {
            Log.e("TTS", "Initilization Failed!");
        }

    }
    private void speakOut() {


        tts.speak(message, TextToSpeech.QUEUE_FLUSH, null);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (tts != null) {
            tts.stop();
            tts.shutdown();
            tts = null;
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser) {
            if (tts != null) {
                tts.stop();
                tts.shutdown();
                tts = null;
            }
        }
    }

}
