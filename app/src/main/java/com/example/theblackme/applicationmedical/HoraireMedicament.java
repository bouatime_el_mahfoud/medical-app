package com.example.theblackme.applicationmedical;

/**
 * Created by theblackme on 07/06/2018.
 */

public class HoraireMedicament {
    private int quantity;
    private String heure;

    public HoraireMedicament(int quantity, String heure) {
        this.quantity = quantity;
        this.heure = heure;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getHeure() {
        return heure;
    }

    public void setHeure(String heure) {
        this.heure = heure;
    }
}
