package com.example.theblackme.applicationmedical;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by theblackme on 06/06/2018.
 */

public class MedicamentPatientArrayAdapter extends ArrayAdapter<MedicamentPatient> {
    public MedicamentPatientArrayAdapter(@NonNull Context context, int resource, @NonNull ArrayList<MedicamentPatient> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View root=convertView;
        if(convertView==null){
            root= LayoutInflater.from(getContext()).inflate(R.layout.listitem_prescription_item,parent,false);
        }
        MedicamentPatient medicament=getItem(position);
        TextView medicament_name=(TextView)root.findViewById(R.id.medicament_name);
        medicament_name.setText(medicament.getNom_medicament());
        TextView patient_name=(TextView)root.findViewById(R.id.patient_name);
        patient_name.setText(medicament.getNom_patient()+" "+medicament.getPrenom_patient());
        return root;
    }
}
