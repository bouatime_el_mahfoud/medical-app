package com.example.theblackme.applicationmedical;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class GestionHoraires extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.gestion_user_container);
        ViewPager v=(ViewPager)findViewById(R.id.container_patients);
        TabLayout tab=(TabLayout)findViewById(R.id.tab_gestion);
        HorairesFragementPagerAdapter h=new HorairesFragementPagerAdapter(getSupportFragmentManager());
        v.setAdapter(h);
        tab.setupWithViewPager(v);
        tab.getTabAt(0).setIcon(R.drawable.toolbar_green);
        tab.getTabAt(1).setIcon(R.drawable.add_rendez);
        tab.setSelectedTabIndicatorColor(getResources().getColor(R.color.materialgreen));
    }
}
