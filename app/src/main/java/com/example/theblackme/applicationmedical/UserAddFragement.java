package com.example.theblackme.applicationmedical;


import android.app.Dialog;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserAddFragement extends Fragment implements TextToSpeech.OnInitListener {
    private ListView listeutilisateurs;
    private String urlv="http://10.23.18.106:7080/appmedical/getallpatients.php";
    private String urla="http://10.23.18.106:7080/appmedical/addpatient.php";
    private RequestQueue requestQueue;
    private StringRequest request;
    private Dialog userAdd;
    private String message;
    private TextToSpeech tts;
    public UserAddFragement() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View root= inflater.inflate(R.layout.users_listview,container,false);
        listeutilisateurs=(ListView) root.findViewById(R.id.listview_usersadd);
        final LinearLayout empty=(LinearLayout)root.findViewById(R.id.emptyview);
        requestQueue= Volley.newRequestQueue(getActivity());
        final ArrayList<Patient> list=new ArrayList<>();
        userAdd=new Dialog(getContext());
        request=new StringRequest(Request.Method.POST, urlv, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{

                    JSONObject objet=new JSONObject(response);
                    Patient prd;
                    JSONArray log=objet.getJSONArray("login");
                    for (int i=0;i<log.length();i++) {

                        JSONObject com=log.getJSONObject(i);
                        prd=new Patient(com.getInt("id_patient"),com.getString("nom"),com.getString("prenom"),com.getString("telephone"),com.getInt("sex"),com.getInt("age"),com.getString("adresse"));
                        list.add(prd);
                    }
                    PatientaddArrayAdapter cmdAdapter=new PatientaddArrayAdapter(getActivity(),0,list);

                    listeutilisateurs.setEmptyView(empty);
                    listeutilisateurs.setAdapter(cmdAdapter);


                }
                catch (JSONException e){
                    Log.e("erroe json","un erreur s'est produit leur de la conversion du json");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(),"une erreur s'est produit coté base de donnée",Toast.LENGTH_LONG).show();
                System.out.println(error);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> params=new HashMap<>();
                params.put("iddoctor",Integer.toString(1));
                return params;
            }
        };
        requestQueue.add(request);
        listeutilisateurs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (tts != null) {
                    tts.stop();
                    tts.shutdown();
                    tts = null;
                }
                final Patient p=list.get(i);
                userAdd.setContentView(R.layout.ajouter_user_popup);
                TextView fullname=(TextView) userAdd.findViewById(R.id.pop_user_add_name);
                TextView address=(TextView) userAdd.findViewById(R.id.user_popup_add_address);
                address.setText(p.getAddress());
                TextView telephone=(TextView) userAdd.findViewById(R.id.pop_user_add_telephone);
                TextView sexe=(TextView) userAdd.findViewById(R.id.user_add_popup_sexe);
                TextView age=(TextView) userAdd.findViewById(R.id.user_add_popup_age);
                TextView logout=(TextView) userAdd.findViewById(R.id.pop_user_add_close) ;
                logout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        userAdd.dismiss();
                    }
                });
                Button add=(Button) userAdd.findViewById(R.id.user_add_popup_addbuttonn);
                fullname.setText(p.getNom()+" "+p.getPrenom());
                telephone.setText(p.getTelephone());
                int sexev=p.getSexe();
                if(sexev==0){
                    sexe.setText("F");
                }
                else if(sexev==1){
                    sexe.setText("M");
                }
                age.setText(Integer.toString(p.getAge()));
                add.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        requestQueue= Volley.newRequestQueue(getActivity());
                        request=new StringRequest(Request.Method.POST, urla, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Toast.makeText(getActivity(),"le patient est ajoutée à votre liste",Toast.LENGTH_LONG).show();
                                getActivity().recreate();
                                userAdd.dismiss();

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getActivity(),"une erreur s'est produit coté base de donnée",Toast.LENGTH_LONG).show();
                                System.out.println(error);
                            }
                        }){
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                HashMap<String,String> params=new HashMap<>();
                                params.put("idmedcin",Integer.toString(1));
                                params.put("idpatient",Integer.toString(p.getId_patient()));
                                return params;
                            }
                        };
                        requestQueue.add(request);


                    }
                });

                userAdd.show();

            }
        });
        listeutilisateurs.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                Patient p=list.get(i);
                if (tts != null) {
                    tts.stop();
                    tts.shutdown();
                    tts = null;
                }
                message="pour acceder au menu d'ajout du patient :"+p.getNom()+" "+p.getPrenom()+" ,veuillez cliquer sur ce bouton";
                tts=new TextToSpeech(getActivity(), UserAddFragement.this);
                speakOut();
                return true;
            }
        });
        empty.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (tts != null) {
                    tts.stop();
                    tts.shutdown();
                    tts = null;
                }
                message="votre liste des patients à ajouter est vide";
                tts=new TextToSpeech(getActivity(), UserAddFragement.this);
                speakOut();
                return true;
            }
        });
        return root;
    }
    @Override
    public void onInit(int i) {
        if (i == TextToSpeech.SUCCESS) {

            int result = tts.setLanguage(Locale.FRANCE);

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {

                speakOut();
            }

        } else {
            Log.e("TTS", "Initilization Failed!");
        }

    }
    private void speakOut() {


        tts.speak(message, TextToSpeech.QUEUE_FLUSH, null);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (tts != null) {
            tts.stop();
            tts.shutdown();
            tts = null;
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser){
            if (tts != null) {
                tts.stop();
                tts.shutdown();
                tts = null;
            }
        }
    }

}
