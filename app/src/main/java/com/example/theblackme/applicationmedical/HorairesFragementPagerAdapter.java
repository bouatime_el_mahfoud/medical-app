package com.example.theblackme.applicationmedical;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by theblackme on 05/06/2018.
 */

public class HorairesFragementPagerAdapter extends FragmentPagerAdapter {

    public HorairesFragementPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return  new HoraireConsultingFragement();
            case 1:
                return  new HoraireAddFragement();
            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "randez-vous";
            case 1:
                return "ajouter randez-vous";
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}
