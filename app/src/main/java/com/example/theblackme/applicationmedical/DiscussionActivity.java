package com.example.theblackme.applicationmedical;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class DiscussionActivity extends AppCompatActivity {
    private int id_doctor,id_patient;
    private ListView  listemessages;
    private TextView  name;
    private EditText message;
    private String fullname;
    private String urlv="http://10.23.18.106:7080/appmedical/getmessagesdoctors.php";
    private String urla="http://10.23.18.106:7080/appmedical/sendmessage.php";
    private RequestQueue requestQueue;
    private ImageView envoyermessage ;
    private StringRequest request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.discussions_layout);
        listemessages=(ListView)findViewById(R.id.listview_message) ;
        message=(EditText) findViewById(R.id.messagea);
        listemessages.setDivider(null);
        listemessages.setDividerHeight(15);
        Bundle extras=getIntent().getExtras();
        if(extras!=null){
            id_doctor=extras.getInt("id_doctor");
            id_patient=extras.getInt("id_patient");
            fullname=extras.getString("fullname");

        }
        name=(TextView)findViewById(R.id.fullname_chat) ;
        envoyermessage=(ImageView) findViewById(R.id.envoyermessge);
        envoyermessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestQueue= Volley.newRequestQueue(DiscussionActivity.this);
                request=new StringRequest(Request.Method.POST, urla, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        message.setText("");
                        message.setHint("Nouveau message");
                        DiscussionActivity.this.recreate();


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(DiscussionActivity.this,"une erreur s'est produit coté base de donnée",Toast.LENGTH_LONG).show();
                        System.out.println(error);
                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        HashMap<String,String> params=new HashMap<>();
                        params.put("idmedcin",Integer.toString(id_doctor));
                        params.put("idpatient",Integer.toString(id_patient));
                        params.put("message",message.getText().toString());
                        return params;
                    }
                };
                requestQueue.add(request);
            }
        });
        name.setText(fullname);
        requestQueue= Volley.newRequestQueue(this);

        final ArrayList<Message> list=new ArrayList<>();
        request=new StringRequest(Request.Method.POST, urlv, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{

                    JSONObject objet=new JSONObject(response);
                    Message prd;
                    JSONArray log=objet.getJSONArray("login");
                    for (int i=0;i<log.length();i++) {

                        JSONObject com=log.getJSONObject(i);

                            prd=new Message(com.getString("message"),com.getString("heure"),com.getInt("id_source"));
                            list.add(prd);
                            MessageArrayAdapter cmdAdapter=new MessageArrayAdapter(DiscussionActivity.this,0,list);

                            listemessages.setAdapter(cmdAdapter);



                    }



                }
                catch (JSONException e){
                    Log.e("erroe json","un erreur s'est produit leur de la conversion du json");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(DiscussionActivity.this,"une erreur s'est produit coté base de donnée",Toast.LENGTH_LONG).show();
                System.out.println(error);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> params=new HashMap<>();
                params.put("iddoctor",Integer.toString(id_doctor));
                params.put("idpatient",Integer.toString(id_patient));

                return params;
            }
        };
        requestQueue.add(request);



    }
}
