package com.example.theblackme.applicationmedical;

/**
 * Created by theblackme on 05/06/2018.
 */

public class PatientDemande {
    private int id_patient;
    private String nom;
    private String prenom;
    private String telephone;
    private int sexe;
    private int age;
    private  int id_demande;
    private String address;

    public PatientDemande(int id_patient, String nom, String prenom, String telephone, int sexe, int age, int id_demande,String address) {
        this.id_patient = id_patient;
        this.nom = nom;
        this.prenom = prenom;
        this.telephone = telephone;
        this.sexe = sexe;
        this.age = age;
        this.id_demande = id_demande;
        this.address=address;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setId_patient(int id_patient) {
        this.id_patient = id_patient;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public void setSexe(int sexe) {
        this.sexe = sexe;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setId_demande(int id_demande) {
        this.id_demande = id_demande;
    }

    public int getId_patient() {
        return id_patient;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getTelephone() {
        return telephone;
    }

    public int getSexe() {
        return sexe;
    }

    public int getAge() {
        return age;
    }

    public int getId_demande() {
        return id_demande;
    }
}
