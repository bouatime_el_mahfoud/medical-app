package com.example.theblackme.applicationmedical;


import android.app.Dialog;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class HoraireConsultingFragement extends Fragment implements TextToSpeech.OnInitListener{

    private ListView listehoraires;
    private String urlv="http://10.23.18.106:7080/appmedical/getrandezvousdoctors.php";
    private String urld="http://10.23.18.106:7080/appmedical/deleterendezvous.php";
    private RequestQueue requestQueue;
    private StringRequest request;
    private Dialog consultingrandez;
    private String message;
    private TextToSpeech tts;

    public HoraireConsultingFragement() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View root=inflater.inflate(R.layout.layout_randezvous_listview,container,false);
        listehoraires=(ListView)root.findViewById(R.id.listview_randezvouus);
        final  LinearLayout empty=(LinearLayout) root.findViewById(R.id.emptyview_randezvous);
        requestQueue= Volley.newRequestQueue(getActivity());
        consultingrandez=new Dialog(getContext());
        final ArrayList<RandezVous> list=new ArrayList<>();
        request=new StringRequest(Request.Method.POST, urlv, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{

                    JSONObject objet=new JSONObject(response);
                    RandezVous prd;
                    JSONArray log=objet.getJSONArray("login");
                        for (int i=0;i<log.length();i++) {

                        JSONObject com=log.getJSONObject(i);
                                prd=new RandezVous(com.getInt("id_rendezvous"),com.getString("sujet"),com.getInt("id_patient"),com.getString("nom"),com.getString("prenom"),com.getString("adresse"),com.getString("heure"));
                                list.add(prd);




                    }
                    RandezVousArrayAddapter cmdAdapter=new RandezVousArrayAddapter(getActivity(),0,list);
                    listehoraires.setEmptyView(empty);
                    listehoraires.setAdapter(cmdAdapter);



                }
                catch (JSONException e){
                    Log.e("erroe json","un erreur s'est produit leur de la conversion du json");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(),"une erreur s'est produit coté base de donnée",Toast.LENGTH_LONG).show();
                System.out.println(error);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> params=new HashMap<>();
                params.put("iddoctor",Integer.toString(1));
                return params;
            }
        };
        requestQueue.add(request);
        listehoraires.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (tts != null) {
                    tts.stop();
                    tts.shutdown();
                    tts = null;
                }
                consultingrandez.setContentView(R.layout.popup_randezvous);
                final RandezVous r=list.get(i);
                TextView fullname=(TextView) consultingrandez.findViewById(R.id.randez_vous_consulting_popup_name);
                TextView address=(TextView) consultingrandez.findViewById(R.id.randez_vous_address_popup);
                address.setText(r.getAddress());
                TextView sujet=(TextView) consultingrandez.findViewById(R.id.randez_vous_sujet_consulting_popup);
                TextView date=(TextView) consultingrandez.findViewById(R.id.randez_vous_date_popup_consulting);
                date.setText("le "+r.getDate());
                TextView close=(TextView) consultingrandez.findViewById(R.id.randez_vous_close_popup_consulting);
                final Button delete=(Button) consultingrandez.findViewById(R.id.randez_vous_delete_button_popup);
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        consultingrandez.dismiss();
                    }
                });
                delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        /*



                         */
                        requestQueue= Volley.newRequestQueue(getActivity());
                        request=new StringRequest(Request.Method.POST, urld, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Toast.makeText(getActivity(),"le rendez_vous est supprimé ",Toast.LENGTH_LONG).show();
                                getActivity().recreate();
                                consultingrandez.dismiss();

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getActivity(),"une erreur s'est produit coté base de donnée",Toast.LENGTH_LONG).show();
                                System.out.println(error);
                            }
                        }){
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                HashMap<String,String> params=new HashMap<>();
                                params.put("idrendez",Integer.toString(r.getId_randezvous()));
                                return params;
                            }
                        };
                        requestQueue.add(request);

                        /*




                         */
                    }
                });
                fullname.setText(r.getNom()+" "+r.getPrenom());
                sujet.setText(r.getSujet());
                consultingrandez.show();


            }
        });
        listehoraires.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                RandezVous r=list.get(i);
                if (tts != null) {
                    tts.stop();
                    tts.shutdown();
                    tts = null;
                }
                message="pour supprimer ou consulter  les informations du rendez-vous du patient:"+ r.getNom()+" "+r.getPrenom()+",dont le sujet est:"+r.getSujet()+",et la date est:le"+r.getDate()+".veuillez cliquer sur ce bouton";
                tts=new TextToSpeech(getActivity(), HoraireConsultingFragement.this);
                speakOut();
                return true;
            }
        });
        empty.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (tts != null) {
                    tts.stop();
                    tts.shutdown();
                    tts = null;
                }
                message="la liste des rendez-vous est vide,pour spécifier un rendez-vous pour un patient de votre liste des patients.Veuillez glisser à gauche";
                tts=new TextToSpeech(getActivity(), HoraireConsultingFragement.this);
                return true;
            }
        });
        return root;

    }
    @Override
    public void onInit(int i) {
        if (i == TextToSpeech.SUCCESS) {

            int result = tts.setLanguage(Locale.FRANCE);

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {

                speakOut();
            }

        } else {
            Log.e("TTS", "Initilization Failed!");
        }

    }
    private void speakOut() {


        tts.speak(message, TextToSpeech.QUEUE_FLUSH, null);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (tts != null) {
            tts.stop();
            tts.shutdown();
            tts = null;
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser) {
            if (tts != null) {
                tts.stop();
                tts.shutdown();
                tts = null;
            }
        }
    }

}
