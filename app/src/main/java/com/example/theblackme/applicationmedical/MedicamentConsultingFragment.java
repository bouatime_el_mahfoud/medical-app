package com.example.theblackme.applicationmedical;


import android.app.Dialog;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.zip.Inflater;

/**
 * A simple {@link Fragment} subclass.
 */
public class MedicamentConsultingFragment extends Fragment implements TextToSpeech.OnInitListener {

private ListView listemedicaments;
    private String urlv="http://10.23.18.106:7080/appmedical/getmedicamentsdoctors.php";
    private String urlh="http://10.23.18.106:7080/appmedical/getmedicamenthoraire.php";
    private String urld="http://10.23.18.106:7080/appmedical/deletemedicaments.php";
    private RequestQueue requestQueue;
    private StringRequest request;
    private Dialog consultingmed;
    private String message;
    private TextToSpeech tts;
    public MedicamentConsultingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View root=inflater.inflate(R.layout.consulting_medicaments_listview,container,false);
        listemedicaments=(ListView)root.findViewById(R.id.listview_medicaments_consulting);
        final LinearLayout empty=(LinearLayout)root.findViewById(R.id.emtyviewmedicaments);
        requestQueue= Volley.newRequestQueue(getActivity());
        consultingmed=new Dialog(getContext());
        final ArrayList<MedicamentPatient> list=new ArrayList<>();
        request=new StringRequest(Request.Method.POST, urlv, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{

                    JSONObject objet=new JSONObject(response);
                    MedicamentPatient prd;
                    JSONArray log=objet.getJSONArray("login");
                    for (int i=0;i<log.length();i++) {


                        JSONObject com=log.getJSONObject(i);
                        prd=new MedicamentPatient(com.getInt("id_medicament"),com.getInt("id_patient"),com.getString("nom_medicament"),com.getString("nom"),com.getString("prenom"));
                        list.add(prd);

                    }
                    MedicamentPatientArrayAdapter cmdAdapter=new MedicamentPatientArrayAdapter(getActivity(),0,list);

                    listemedicaments.setEmptyView(empty);
                    listemedicaments.setAdapter(cmdAdapter);


                }
                catch (JSONException e){
                    Log.e("erroe json","un erreur s'est produit leur de la conversion du json");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(),"une erreur s'est produit coté base de donnée",Toast.LENGTH_LONG).show();
                System.out.println(error);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> params=new HashMap<>();
                params.put("iddoctor",Integer.toString(1));
                return params;
            }
        };
        requestQueue.add(request);
        listemedicaments.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (tts != null) {
                    tts.stop();
                    tts.shutdown();
                    tts = null;
                }
                final MedicamentPatient m=list.get(i);
                final HoraireMedicament horaire;
                consultingmed.setContentView(R.layout.consulting_medicament_pop_up);
                final LinearLayout parentView=(LinearLayout)consultingmed.findViewById(R.id.parentView_medicamentConsulting);
                TextView fullname=(TextView)consultingmed.findViewById(R.id.consulting_medicament_popup_name);
                TextView nammed=(TextView)consultingmed.findViewById(R.id.consulting_medicament_nom_medicament_popup);
                final TextView quantite=(TextView)consultingmed.findViewById(R.id.consultation_medicament_quantity_popup);
                TextView close=(TextView) consultingmed.findViewById(R.id.consulting_medicament_close_popup) ;
                final TextView date=(TextView)consultingmed.findViewById(R.id.consulting_medicament_date_popup);
                Button delete=(Button) consultingmed.findViewById(R.id.consultation_medicament_deletebutton);
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        consultingmed.dismiss();
                    }
                });
                delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        requestQueue= Volley.newRequestQueue(getActivity());
                        request=new StringRequest(Request.Method.POST, urld, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Toast.makeText(getActivity(),"le medicament est supprimé ",Toast.LENGTH_LONG).show();
                                getActivity().recreate();
                                consultingmed.dismiss();

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getActivity(),"une erreur s'est produit coté base de donnée",Toast.LENGTH_LONG).show();
                                System.out.println(error);
                            }
                        }){
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                HashMap<String,String> params=new HashMap<>();
                                params.put("idmedicament",Integer.toString(m.getId_medicament()));
                                return params;
                            }
                        };
                        requestQueue.add(request);
                    }
                });
                fullname.setText(m.getNom_patient()+" "+m.getPrenom_patient());
                nammed.setText(m.getNom_medicament());
                /*il rest l'initialisation des champs*/
                request=new StringRequest(Request.Method.POST, urlh, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try{

                            JSONObject objet=new JSONObject(response);
                            HoraireMedicament horaire;
                            ArrayList<HoraireMedicament> listm=new ArrayList<>();
                            JSONArray log=objet.getJSONArray("login");
                            for (int i=0;i<log.length();i++) {
                                JSONObject com=log.getJSONObject(i);
                                horaire=new HoraireMedicament(com.getInt("quantite_prise"),com.getString("heure"));
                                listm.add(horaire);
                            }

                            date.setText(listm.get(0).getHeure());
                            quantite.setText(Integer.toString(listm.get(0).getQuantity()));
                            for (int i=1;i<listm.size();i++){
                                View view=inflater.inflate(R.layout.childconsultin_pop_up_medicament,null);
                                TextView heurex=(TextView)view.findViewById(R.id.heure_consulting_child) ;
                                heurex.setText(listm.get(i).getHeure());

                                parentView.addView(view);
                            }


                        }
                        catch (JSONException e){
                            Log.e("erroe json","un erreur s'est produit leur de la conversion du json");
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(),"une erreur s'est produit coté base de donnée",Toast.LENGTH_LONG).show();
                        System.out.println(error);
                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        HashMap<String,String> params=new HashMap<>();
                        params.put("idmedicament",Integer.toString(m.getId_medicament()));
                        return params;
                    }
                };
               ;
                requestQueue.add(request);
                consultingmed.show();

            }
        });
        listemedicaments.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (tts != null) {
                    tts.stop();
                    tts.shutdown();
                    tts = null;
                }
                MedicamentPatient m=list.get(i);
                message="pour supprimer ou consulter  les informations du médicament:"+m.getNom_medicament()+" du patient:"+m.getNom_patient()+" "+m.getPrenom_patient()+" .Veuillez cliquer sur ce bouton";
                tts=new TextToSpeech(getActivity(), MedicamentConsultingFragment.this);
                speakOut();
                return true;
            }
        });
        empty.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                message="la liste des médicaments spécifié est vide.Pour spécifier un médicament pour un patient.Glisser à gauche";
                tts=new TextToSpeech(getActivity(), MedicamentConsultingFragment.this);
                speakOut();
                return true;
            }
        });
        return root;
    }
    @Override
    public void onInit(int i) {
        if (i == TextToSpeech.SUCCESS) {

            int result = tts.setLanguage(Locale.FRANCE);

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {

                speakOut();
            }

        } else {
            Log.e("TTS", "Initilization Failed!");
        }

    }
    private void speakOut() {


        tts.speak(message, TextToSpeech.QUEUE_FLUSH, null);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (tts != null) {
            tts.stop();
            tts.shutdown();
            tts = null;
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser){
            if (tts != null) {
                tts.stop();
                tts.shutdown();
                tts = null;
            }
        }
    }

}
