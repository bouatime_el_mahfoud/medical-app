package com.example.theblackme.applicationmedical;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class GestionMedicament extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gestion_user_container);
        ViewPager v=(ViewPager)findViewById(R.id.container_patients);
        TabLayout tab=(TabLayout)findViewById(R.id.tab_gestion);
        MedicamentsFragementManagerAdapter pager=new MedicamentsFragementManagerAdapter(getSupportFragmentManager());
        v.setAdapter(pager);
        tab.setupWithViewPager(v);
        tab.getTabAt(0).setIcon(R.drawable.medicamentconsulting);
        tab.getTabAt(1).setIcon(R.drawable.ajoutermedicament);
        tab.setSelectedTabIndicatorColor(getResources().getColor(R.color.materialyellow));


    }

}
