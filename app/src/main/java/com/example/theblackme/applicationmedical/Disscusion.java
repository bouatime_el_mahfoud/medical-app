package com.example.theblackme.applicationmedical;

import java.util.Date;

/**
 * Created by theblackme on 06/06/2018.
 */

public class Disscusion {
    private int id_patientmessage;
    private String message;
    private String nom;
    private String prenom;
    private String datemessage;

    public Disscusion(int id_patientmessage, String message, String nom, String prenom, String datemessage) {
        this.id_patientmessage = id_patientmessage;
        this.message = message;

        this.nom = nom;
        this.prenom = prenom;
        this.datemessage = datemessage;
    }

    public void setId_patientmessage(int id_patientmessage) {
        this.id_patientmessage = id_patientmessage;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setDatemessage(String datemessage) {
        this.datemessage = datemessage;
    }

    public int getId_patientmessage() {
        return id_patientmessage;
    }

    public String getMessage() {
        return message;
    }


    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getDatemessage() {
        return datemessage;
    }
}
