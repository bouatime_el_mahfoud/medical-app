package com.example.theblackme.applicationmedical;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class SettingsActivity extends AppCompatActivity {
    private EditText formations;
    private EditText email;
    private EditText address;
    private EditText number;
    private Button valider;
    private Button quitter;
    private RequestQueue requestQueue;
    private StringRequest request;
    private String urla="http://10.23.18.106:7080/appmedical/settingsadmin.php";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
        formations=findViewById(R.id.formations);
        email=findViewById(R.id.email);
        address=findViewById(R.id.addresss);
        number=findViewById(R.id.phone);
        valider=(Button)findViewById(R.id.valider);
        quitter=(Button)findViewById(R.id.quit);
        quitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!formations.getText().toString().isEmpty()&&!email.getText().toString().isEmpty()&&!address.getText().toString().isEmpty()&&!number.getText().toString().isEmpty()){
                    requestQueue= Volley.newRequestQueue(SettingsActivity.this);
                    request=new StringRequest(Request.Method.POST, urla, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            Toast.makeText(SettingsActivity.this,"modifié ",Toast.LENGTH_LONG).show();
                            finish();



                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(SettingsActivity.this,"une erreur s'est produit coté base de donnée",Toast.LENGTH_LONG).show();
                            System.out.println(error);
                        }
                    }){
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            HashMap<String,String> params=new HashMap<>();
                            params.put("description",formations.getText().toString());
                            params.put("idmedcin",Integer.toString(1));
                            params.put("mail",email.getText().toString());
                            params.put("telephone",number.getText().toString());
                            params.put("addresse",address.getText().toString());
                            return params;
                        }
                    };
                    requestQueue.add(request);
                }
                else{
                    Toast.makeText(SettingsActivity.this,"au moins un champ est vide",Toast.LENGTH_LONG).show();
                }

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
