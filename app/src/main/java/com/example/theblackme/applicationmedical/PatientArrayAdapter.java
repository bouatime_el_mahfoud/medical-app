package com.example.theblackme.applicationmedical;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by theblackme on 05/06/2018.
 */

public class PatientArrayAdapter extends ArrayAdapter<Patient> {
    public PatientArrayAdapter(@NonNull Context context, int resource, ArrayList<Patient> patients) {
        super(context, resource, patients);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
       View root=convertView;
       if(convertView==null){
           root= LayoutInflater.from(getContext()).inflate(R.layout.users_items,parent,false);
       }
       Patient patient=getItem(position);
        TextView nom=root.findViewById(R.id.nomuserconsulting);
        nom.setText(patient.getNom());
        TextView prenom=root.findViewById(R.id.prenomuserconsulting);
        prenom.setText(patient.getPrenom());
        return root;

    }
}
