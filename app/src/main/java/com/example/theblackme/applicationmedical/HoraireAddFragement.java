package com.example.theblackme.applicationmedical;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class HoraireAddFragement extends Fragment implements TextToSpeech.OnInitListener {
    private ListView listeview;
    private Dialog addrandezvous;
    private String urlv="http://10.23.18.106:7080/appmedical/getpatientsofdoctors.php";
    private String urla="http://10.23.18.106:7080/appmedical/addrendezvous.php";
    private RequestQueue requestQueue;
    private StringRequest request;
    private DatePicker datePicker;
    int day,month,year;
    private Calendar calendar;
    private Calendar c;
    private DatePicker dp;
    String datetime;
    private String message;
    private TextToSpeech tts;
    public HoraireAddFragement() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View root=inflater.inflate(R.layout.add_randez_vous_listview,container,false);
        listeview=(ListView) root.findViewById(R.id.listview_randezvouus_add);
        requestQueue= Volley.newRequestQueue(getActivity());
        final LinearLayout empty=(LinearLayout)root.findViewById(R.id.emptyviewaddhoraire);
        final ArrayList<Patient> list=new ArrayList<>();
        addrandezvous=new Dialog(getContext());
        request=new StringRequest(Request.Method.POST, urlv, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{

                    JSONObject objet=new JSONObject(response);
                    Patient prd;
                    JSONArray log=objet.getJSONArray("login");
                    for (int i=0;i<log.length();i++) {

                        JSONObject com=log.getJSONObject(i);
                        prd=new Patient(com.getInt("id_patient"),com.getString("nom"),com.getString("prenom"),com.getString("telephone"),com.getInt("sex"),com.getInt("age"),com.getString("adresse"));
                        list.add(prd);
                    }
                    RandezVousAddArrayAdapter cmdAdapter=new RandezVousAddArrayAdapter(getActivity(),0,list);

                    listeview.setEmptyView(empty);
                    listeview.setAdapter(cmdAdapter);


                }
                catch (JSONException e){
                    Log.e("erroe json","un erreur s'est produit leur de la conversion du json");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(),"une erreur s'est produit coté base de donnée",Toast.LENGTH_LONG).show();
                System.out.println(error);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> params=new HashMap<>();
                params.put("iddoctor",Integer.toString(1));
                return params;
            }
        };
        requestQueue.add(request);
        listeview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (tts != null) {
                    tts.stop();
                    tts.shutdown();
                    tts = null;
                }
               final Patient p=list.get(i);
                addrandezvous.setContentView(R.layout.popup_set_randez_vous);
                final EditText sujet=addrandezvous.findViewById(R.id.set_randezvous_popup_sujet);
                TextView fullname=(TextView)addrandezvous.findViewById(R.id.set_randezvous_popup_name);
                final TextView datetext=(TextView) addrandezvous.findViewById(R.id.set_randezvous_datetext_pop_up);
                LinearLayout date=(LinearLayout) addrandezvous.findViewById(R.id.setrandezvous_date_popup);
                final TextView close=(TextView) addrandezvous.findViewById(R.id.set_randezvous_close_popup);
                Button confirmer=(Button) addrandezvous.findViewById(R.id.setrandezvous_popup_confirmerbutton);
                confirmer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(!datetext.getText().toString().equalsIgnoreCase("aucune date specifie")&& !sujet.getText().toString().isEmpty()){
                            requestQueue= Volley.newRequestQueue(getActivity());
                            request=new StringRequest(Request.Method.POST, urla, new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Toast.makeText(getActivity(),"le rendez_vous est ajouté ",Toast.LENGTH_LONG).show();
                                    getActivity().recreate();
                                    addrandezvous.dismiss();

                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Toast.makeText(getActivity(),"une erreur s'est produit coté base de donnée",Toast.LENGTH_LONG).show();
                                    System.out.println(error);
                                }
                            }){
                                @Override
                                protected Map<String, String> getParams() throws AuthFailureError {
                                    HashMap<String,String> params=new HashMap<>();
                                    params.put("idpatient",Integer.toString(p.getId_patient()));
                                    params.put("idmedcin",Integer.toString(1));
                                    String sujete="'"+sujet.getText().toString()+"'";
                                    params.put("sujet",sujete);
                                    String datee="'"+datetext.getText().toString()+"'";
                                    params.put("date",datee);
                                    return params;
                                }
                            };
                            requestQueue.add(request);
                        }
                        else{
                            Toast.makeText(getContext(),"veuillez remplir tous les champs",Toast.LENGTH_SHORT).show();

                        }
                    }
                });
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        addrandezvous.dismiss();
                    }
                });
                fullname.setText(p.getNom()+" "+p.getPrenom());
                date.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                      /*


                       */
                    calendar=Calendar.getInstance();
                    dp=(DatePicker)addrandezvous.findViewById(R.id.datepicker);
                    dp=new DatePicker(getContext());
                    final Dialog d=new Dialog(getContext());
                    d.setContentView(R.layout.datetime_layout);
                    ImageView add=(ImageView)d.findViewById(R.id.closedatepicker);
                    add.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            d.dismiss();
                        }
                    });
                    ImageView closedate=(ImageView)d.findViewById(R.id.movetotime);
                    closedate.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            datetime=dp.getYear()+"-"+dp.getMonth()+"-"+dp.getDayOfMonth()+" ";
                            d.setContentView(R.layout.time_picker_layout);
                            final TimePicker t=(TimePicker)d.findViewById(R.id.time_picker);
                            t.setIs24HourView(true);
                             ImageView confirmdatetime=(ImageView)d.findViewById(R.id.confirm_time);
                            confirmdatetime.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    datetime=datetime+t.getHour()+":"+t.getMinute();
                                    System.out.println(datetime);
                                    datetext.setText(datetime);
                                    d.dismiss();

                                }
                            });
                           ImageView annuler=(ImageView)d.findViewById(R.id.cancel_time);
                           annuler.setOnClickListener(new View.OnClickListener() {
                               @Override
                               public void onClick(View view) {
                                   d.dismiss();
                               }
                           });
                        }
                    });


                    d.show();

                      /*



                       */
                    }
                });
                addrandezvous.show();


            }
        });
        listeview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                Patient p=list.get(i);
                if (tts != null) {
                    tts.stop();
                    tts.shutdown();
                    tts = null;
                }
                message="pour spécifier un rendez-vous pour le patient:"+p.getNom()+" "+p.getPrenom()+".Veuillez cliquer sur ce bouton";
                tts=new TextToSpeech(getActivity(), HoraireAddFragement.this);
                return true;
            }
        });
        empty.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (tts != null) {
                    tts.stop();
                    tts.shutdown();
                    tts = null;
                }
                message="votre liste des patients est vide";
                tts=new TextToSpeech(getActivity(), HoraireAddFragement.this);
                return true;
            }
        });
        return root;



    }
    @Override
    public void onInit(int i) {
        if (i == TextToSpeech.SUCCESS) {

            int result = tts.setLanguage(Locale.FRANCE);

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {

                speakOut();
            }

        } else {
            Log.e("TTS", "Initilization Failed!");
        }

    }
    private void speakOut() {


        tts.speak(message, TextToSpeech.QUEUE_FLUSH, null);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (tts != null) {
            tts.stop();
            tts.shutdown();
            tts = null;
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser) {
            if (tts != null) {
                tts.stop();
                tts.shutdown();
                tts = null;
            }
        }
    }

}
