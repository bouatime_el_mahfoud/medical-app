package com.example.theblackme.applicationmedical;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by theblackme on 05/06/2018.
 */

public class RandezVousArrayAddapter extends ArrayAdapter<RandezVous> {
    public RandezVousArrayAddapter(@NonNull Context context, int resource, @NonNull ArrayList<RandezVous> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View root=convertView;
        if(convertView==null){
            root= LayoutInflater.from(getContext()).inflate(R.layout.randezvous_item,parent,false);

        }
        RandezVous randezVous=getItem(position);
        TextView sujet=(TextView)root.findViewById(R.id.sujet_randez_vous);
        sujet.setText(randezVous.getSujet());
        TextView date=(TextView) root.findViewById(R.id.date_randez_vous);
        date.setText(randezVous.getDate());
        return root;

    }
}
