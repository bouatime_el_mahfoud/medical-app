package com.example.theblackme.applicationmedical;


import android.app.Dialog;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class MedicamentAddFragment extends Fragment implements TextToSpeech.OnInitListener {

    private ListView listeMedicaments;
    private String heureverif;
    private Dialog addmed;
    private String urlv="http://10.23.18.106:7080/appmedical/getpatientsofdoctors.php";
    private String urla="http://10.23.18.106:7080/appmedical/addmedicaments.php";
    private RequestQueue requestQueue;
    private StringRequest request;
    private String message;
    private TextToSpeech tts;
    public MedicamentAddFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View root=inflater.inflate(R.layout.add_medicaments_listview,container,false);
        listeMedicaments=(ListView)root.findViewById(R.id.listview_medicaments_add);
        addmed=new Dialog(getContext());
        final LinearLayout empty=(LinearLayout)root.findViewById(R.id.emtyviewmedicamentsadd);
        requestQueue= Volley.newRequestQueue(getActivity());
        final ArrayList<Patient> list=new ArrayList<>();
        request=new StringRequest(Request.Method.POST, urlv, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{

                    JSONObject objet=new JSONObject(response);
                    Patient prd;
                    JSONArray log=objet.getJSONArray("login");
                    for (int i=0;i<log.length();i++) {

                        JSONObject com=log.getJSONObject(i);
                        prd=new Patient(com.getInt("id_patient"),com.getString("nom"),com.getString("prenom"),com.getString("telephone"),com.getInt("sex"),com.getInt("age"),com.getString("adresse"));
                        list.add(prd);
                    }
                    MedicamentAddArrayAdapter cmdAdapter=new MedicamentAddArrayAdapter(getActivity(),0,list);

                    listeMedicaments.setEmptyView(empty);
                    listeMedicaments.setAdapter(cmdAdapter);


                }
                catch (JSONException e){
                    Log.e("erroe json","un erreur s'est produit leur de la conversion du json");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(),"une erreur s'est produit coté base de donnée",Toast.LENGTH_LONG).show();
                System.out.println(error);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String,String> params=new HashMap<>();
                params.put("iddoctor",Integer.toString(1));
                return params;
            }
        };
        requestQueue.add(request);
        listeMedicaments.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                addmed.setContentView(R.layout.pop_up_ajouter_prescription);
                final Patient p=list.get(i);
                TextView fullname=(TextView)addmed.findViewById(R.id.medicament_add_name_popup);
                TextView close=(TextView) addmed.findViewById(R.id.medicament_add_close_popup) ;
                final TextView date=(TextView)addmed.findViewById(R.id.medicament_add_datetext_popup);
                date.setTag("time");
                heureverif=date.getText().toString();
               final EditText nommedicament=(EditText)addmed.findViewById(R.id.medicament_add_nommed_popup);
                Button specifierHeure=(Button) addmed.findViewById(R.id.medicament_add_specifierdate_popup);
                specifierHeure.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final Dialog d=new Dialog(getContext());
                        d.setContentView(R.layout.time_picker_layout);
                        final TimePicker timePicker=d.findViewById(R.id.time_picker);
                        timePicker.setIs24HourView(true);
                        ImageView confirm=(ImageView)d.findViewById(R.id.confirm_time);
                        ImageView cancel=(ImageView)d.findViewById(R.id.cancel_time);
                        cancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                d.dismiss();
                            }
                        });
                        confirm.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                date.setText(Integer.toString(timePicker.getHour())+":"+Integer.toString(timePicker.getMinute()));
                                heureverif=Integer.toString(timePicker.getHour())+":"+Integer.toString(timePicker.getMinute());
                                d.dismiss();
                            }
                        });
                        d.show();
                    }
                });
                Button addHeure=(Button) addmed.findViewById(R.id.medicament_add_nouveaudatebutton_popup);
                Button addMedicament=(Button) addmed.findViewById(R.id.medicament_add_addmedicament_popup);
                addMedicament.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        LinearLayout l=(LinearLayout)addmed.findViewById(R.id.parentofall) ;
                        final ArrayList<TextView> times=getViewsByTag(l,"time");
                        boolean iscorrect=true;
                        int i=0;
                        while (i<times.size()&&iscorrect==true){
                            TextView datechild=times.get(i);
                            String textdeladate=datechild.getText().toString();
                            if(textdeladate.equalsIgnoreCase("aucune heure specifie")){
                                iscorrect=false;
                            }
                            i++;
                        }
                        if (iscorrect==true && !nommedicament.getText().toString().isEmpty()){
                            requestQueue= Volley.newRequestQueue(getActivity());
                            request=new StringRequest(Request.Method.POST, urla, new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Toast.makeText(getActivity(),"le medicament est ajouté ",Toast.LENGTH_LONG).show();
                                    getActivity().recreate();
                                    addmed.dismiss();

                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Toast.makeText(getActivity(),"une erreur s'est produit coté base de donnée",Toast.LENGTH_LONG).show();
                                    System.out.println(error);
                                }
                            }){
                                @Override
                                protected Map<String, String> getParams() throws AuthFailureError {
                                    HashMap<String,String> params=new HashMap<>();
                                    params.put("idpatient",Integer.toString(p.getId_patient()));
                                    params.put("nommedicament",nommedicament.getText().toString());
                                    params.put("quantity",Integer.toString(times.size()));
                                    params.put("idmedcin",Integer.toString(1));
                                    for(int i=1;i<times.size()+1;i++){
                                        params.put("param"+i,times.get(i-1).getText().toString());
                                    }
                                    return params;
                                }
                            };
                            requestQueue.add(request);

                        }
                        else if (nommedicament.getText().toString().isEmpty()){
                            Toast.makeText(getContext(),"le nom du medicament n'est pas specifié",Toast.LENGTH_LONG).show();
                        }
                        else {
                            Toast.makeText(getContext(),"le champ numero: "+Integer.toString(times.size())+"n'est pas specifié",Toast.LENGTH_LONG).show();
                        }

                    }
                });
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        addmed.dismiss();
                    }
                });
                addHeure.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final LinearLayout parentview=(LinearLayout)addmed.findViewById(R.id.parentview_add) ;
                        final View row=inflater.inflate(R.layout.child_field_medicame_add,null);
                        if (heureverif.equalsIgnoreCase("aucune heure specifie")){
                            Toast.makeText(getContext(),"veuillez specifier l'heure avant d'ajouter un nouveau champ",Toast.LENGTH_LONG).show();
                        }
                        else{
                            parentview.addView(row);
                            heureverif="Aucune heure specifie";
                        }
                        ImageView addheure=(ImageView)row.findViewById(R.id.add_heure_add_medicament);
                        ImageView deleterow=(ImageView)row.findViewById(R.id.delete_row);
                        final TextView time=(TextView) row.findViewById(R.id.text_child) ;
                        time.setTag("time");
                        addheure.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                final Dialog d=new Dialog(getContext());
                                d.setContentView(R.layout.time_picker_layout);
                                final TimePicker timePicker=d.findViewById(R.id.time_picker);
                                timePicker.setIs24HourView(true);
                                ImageView confirm=(ImageView)d.findViewById(R.id.confirm_time);
                                ImageView cancel=(ImageView)d.findViewById(R.id.cancel_time);
                                cancel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        d.dismiss();
                                    }
                                });
                                confirm.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        time.setText(Integer.toString(timePicker.getHour())+":"+Integer.toString(timePicker.getMinute()));
                                        heureverif=Integer.toString(timePicker.getHour())+":"+Integer.toString(timePicker.getMinute());
                                        d.dismiss();
                                    }
                                });
                                d.show();

                            }
                        });
                        deleterow.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                parentview.removeView(row);
                                heureverif="";
                            }
                        });

                    }
                });

                fullname.setText(p.getNom()+" "+p.getPrenom());
                addmed.show();

            }

        });
        listeMedicaments.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                Patient p=list.get(i);
                if (tts != null) {
                    tts.stop();
                    tts.shutdown();
                    tts = null;
                }
                message="pour spécifier un médicament pour le patient:"+p.getNom()+" "+p.getPrenom()+".Veuillez cliquer sur ce bouton";
                tts=new TextToSpeech(getActivity(), MedicamentAddFragment.this);
                return true;
            }
        });
        empty.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (tts != null) {
                    tts.stop();
                    tts.shutdown();
                    tts = null;
                }
                message="votre liste des patients est vide";
                tts=new TextToSpeech(getActivity(), MedicamentAddFragment.this);
                return true;
            }
        });
        return root;
    }
    private static ArrayList<TextView> getViewsByTag(ViewGroup root, String tag){
        ArrayList<TextView> views = new ArrayList<TextView>();
        final int childCount = root.getChildCount();
        for (int i = 0; i < childCount; i++) {
            final View child = root.getChildAt(i);
            if (child instanceof ViewGroup) {
                views.addAll(getViewsByTag((ViewGroup) child, tag));
            }

            final Object tagObj = child.getTag();
            if (tagObj != null && tagObj.equals(tag)) {
                views.add((TextView)child);
            }

        }
        return views;
    }
    @Override
    public void onInit(int i) {
        if (i == TextToSpeech.SUCCESS) {

            int result = tts.setLanguage(Locale.FRANCE);

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {

                speakOut();
            }

        } else {
            Log.e("TTS", "Initilization Failed!");
        }

    }
    private void speakOut() {


        tts.speak(message, TextToSpeech.QUEUE_FLUSH, null);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (tts != null) {
            tts.stop();
            tts.shutdown();
            tts = null;
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser){
            if (tts != null) {
                tts.stop();
                tts.shutdown();
                tts = null;
            }
        }
    }
}
