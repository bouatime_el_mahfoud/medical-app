<?php

if($_SERVER["REQUEST_METHOD"] == "GET") {

    include 'connection.php';
    $id_patient = $_GET["idp"];
    $id_medecin = $_GET["idm"];
    getDemande($id_patient, $id_medecin);
}

function getDemande($id_patient, $id_medecin) {

    global $connect;

    $query = " select * from demande where id_patient='$id_patient' and id_medecin='$id_medecin' ";

    $result = mysqli_query($connect, $query);
    $number_of_rows = mysqli_num_rows($result);

    $temp_array = array();

    if($number_of_rows > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            $temp_array[] = $row;
        }
    }

    header('Content-Type: application\json');
    echo json_encode(array("demande"=>$temp_array));
    mysqli_close($connect);

}
