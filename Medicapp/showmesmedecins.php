<?php

if($_SERVER["REQUEST_METHOD"] == "GET") {

    include 'connection.php';
    $id_patient=$_GET["idp"];
    getMedecins($id_patient);
}

function getMedecins($id_patient) {

    global $connect;

    $query = " select medecin.id_medecin,medecin.nom,medecin.prenom,medecin.specialite,medecin.mail,medecin.telephone,demande.etat from medecin,demande where demande.id_medecin=medecin.id_medecin and demande.id_patient='$id_patient' and demande.etat='Acceptee'  ";

    $result = mysqli_query($connect, $query);
    $number_of_rows = mysqli_num_rows($result);

    $temp_array = array();

    if($number_of_rows > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            $temp_array[] = $row;
        }
    }

    header('Content-Type: application\json');
    echo json_encode(array("medecins"=>$temp_array));
    mysqli_close($connect);

}
