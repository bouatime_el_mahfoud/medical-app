<?php

if($_SERVER["REQUEST_METHOD"] == "GET") {

    include 'connection.php';
    $id_patient = $_GET["idp"];

    getMedicaments($id_patient);
}

function getMedicaments($id_patient) {

    global $connect;

    $query = "select  medicament.id_medicament, medicament.nom_medicament AS nommedicament,medicament.quantite_prise ,medicament.dose,medecin.nom AS nommedecin,medecin.prenom AS prenommedecin from medicament,prescription,medecin where medicament.id_prescription=prescription.id_prescription and medecin.id_medecin=prescription.id_medecin and prescription.id_patient='$id_patient' ";

    $result = mysqli_query($connect, $query);
    $number_of_rows = mysqli_num_rows($result);

    $temp_array = array();

    if($number_of_rows > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            $temp_array[] = $row;
        }
    }

    header('Content-Type: application\json');
    echo json_encode(array("medicaments"=>$temp_array));
    mysqli_close($connect);

}
