<?php

if($_SERVER["REQUEST_METHOD"] == "GET") {

    include 'connection.php';
    $id_medicament = $_GET["idm"];

    getHoraires($id_medicament);
}

function getHoraires($id_medicament) {

    global $connect;

    $query = " select id_horaire,heure from horaire where id_medicament='$id_medicament' ";

    $result = mysqli_query($connect, $query);
    $number_of_rows = mysqli_num_rows($result);

    $temp_array = array();

    if($number_of_rows > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            $temp_array[] = $row;
        }
    }

    header('Content-Type: application\json');
    echo json_encode(array("horaires"=>$temp_array));
    mysqli_close($connect);

}
