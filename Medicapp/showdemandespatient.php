<?php

if($_SERVER["REQUEST_METHOD"] == "GET") {

    include 'connection.php';
    $id_patient = $_GET["idp"];

    getDemandes($id_patient);
}

function getDemandes($id_patient) {

    global $connect;

    $query = " select demande.id_demande,patient.nom AS nompatient,patient.prenom AS prenompatient,medecin.id_medecin,medecin.nom AS nommedecin,medecin.prenom AS prenommedecin,demande.etat from patient,medecin,demande where patient.id_patient=demande.id_patient and medecin.id_medecin=demande.id_medecin and demande.id_patient='$id_patient' ";

    $result = mysqli_query($connect, $query);
    $number_of_rows = mysqli_num_rows($result);

    $temp_array = array();

    if($number_of_rows > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            $temp_array[] = $row;
        }
    }

    header('Content-Type: application\json');
    echo json_encode(array("demandes"=>$temp_array));
    mysqli_close($connect);

}
