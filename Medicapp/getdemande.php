<?php

if($_SERVER["REQUEST_METHOD"] == "GET") {

    include 'connection.php';
    $id_demande = $_GET["iddemande"];

    getDemande($id_demande);
}

function getDemande($id_demande) {

    global $connect;

    $query = "select id_medecin,etat from demande where id_demande=$id_demande";

    $result = mysqli_query($connect, $query);
    $number_of_rows = mysqli_num_rows($result);

    $temp_array = array();

    if($number_of_rows > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            $temp_array[] = $row;
        }
    }

    header('Content-Type: application\json');
    echo json_encode(array("demande"=>$temp_array));
    mysqli_close($connect);

}
