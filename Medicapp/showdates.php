<?php

if($_SERVER["REQUEST_METHOD"] == "GET") {

    include 'connection.php';
    $id = $_GET["id"];

    getDates($id);
}

function getDates($id) {

    global $connect;

    $query = "select rendezvous.id_rendezvous AS idrendezvous,medecin.nom AS nommedecin,medecin.prenom AS prenommedecin,rendezvous.sujet AS sujet,rendezvous.heure AS date from rendezvous,medecin where  rendezvous.id_medecin=medecin.id_medecin and rendezvous.id_patient='$id' ";

    $result = mysqli_query($connect, $query);
    $number_of_rows = mysqli_num_rows($result);

    $temp_array = array();

    if($number_of_rows > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            $temp_array[] = $row;
        }
    }

    header('Content-Type: application\json');
    echo json_encode(array("rendezvous"=>$temp_array));
    mysqli_close($connect);

}
